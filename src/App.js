import React, { useRef } from "react";

import Body from "./components/Body/Body";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";


const App = () => {
  const phoneNumber = '+7 (988)-718-82-88';
  const carouselRef = useRef(null);

  return (
    <div>
      <Header carouselRef={carouselRef} />
      <Body phone={phoneNumber} carouselRef={carouselRef} />
      <Footer phone={phoneNumber} />
    </div>
  );
}

export default App;
