import React from 'react';

import styles from './Body.module.css';
import CityList from "../CityList/CityList";
import Description from "../Description/Description";
import CarouselAuto from "../CarouselAuto/CarouselAuto";

const Body = ({ phone, carouselRef }) => {
  return (
    <div className={styles.wrapper}>
      <Description phone={phone} />
      <CityList />
      <CarouselAuto carouselRef={carouselRef} />
    </div>
  );
};

export default Body;