import React, { useState } from 'react';
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import styles from './CarouselAuto.module.css';

import {
  EconomProps,
  ComfortProps,
  BusinessProps,
  MinivanProps,
  MicrobusProps
} from "./CarouselAuto.props";

/* Жесткий говнокод, нужно отрефакторить */

export let isMobile = false;

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
  .test(navigator.userAgent)) {
  isMobile = true;
}

const Econom = () => {
  return (
    <Carousel showArrows={!isMobile} showThumbs={false}>
      {EconomProps.map(item => (
        <div key={item.carName} className={styles.imgWrapper}>
          <img src={item.image} alt={item.carName} className={styles.img} title={item.carName} />
          {isMobile ? null : (<p className="legend">{item.carName}</p>)}
        </div>
      ))}
    </Carousel>
  );
};

const Comfort = () => {
  return (
    <Carousel showArrows={!isMobile} showThumbs={false}>
      {ComfortProps.map(item => (
        <div key={item.carName} className={styles.imgWrapper} title={item.carName}>
          <img src={item.image} alt={item.carName} className={styles.img} />
          {isMobile ? null : (<p className="legend">{item.carName}</p>)}
        </div>
      ))}
    </Carousel>
  );
};

const Business = () => {
  return (
    <Carousel showArrows={!isMobile} showThumbs={false}>
      {BusinessProps.map(item => (
        <div key={item.carName} className={styles.imgWrapper} title={item.carName}>
          <img src={item.image} alt={item.carName} className={styles.img} />
          {isMobile ? null : (<p className="legend">{item.carName}</p>)}
        </div>
      ))}
    </Carousel>
  );
};

const Minivan = () => {
  return (
    <Carousel showArrows={!isMobile} showThumbs={false}>
      {MinivanProps.map(item => (
        <div key={item.carName} className={styles.imgWrapper} title={item.carName}>
          <img src={item.image} alt={item.carName} className={styles.img} />
          {isMobile ? null : (<p className="legend">{item.carName}</p>)}
        </div>
      ))}
    </Carousel>
  );
};

const Microbus = () => {
  return (
    <Carousel showArrows={!isMobile} showThumbs={false}>
      {MicrobusProps.map(item => (
        <div key={item.carName} className={styles.imgWrapper} title={item.carName}>
          <img src={item.image} alt={item.carName} className={styles.img} />
          {isMobile ? null : (<p className="legend">{item.carName}</p>)}
        </div>
      ))}
    </Carousel>
  );
};

const CarouselAuto = ({ carouselRef }) => {
  const [componentName, setComponentName] = useState('econom');

  const renderComponent = () => {
    switch (componentName) {
      case 'econom':
        return <Econom />;
      case 'comfort':
        return <Comfort />;
      case 'business':
        return <Business />;
      case 'minivan':
        return <Minivan />;
      case 'microbus':
        return <Microbus />;
      default:
        return;
    }
  };

  return (
    <div ref={carouselRef} className={styles.carouselWrapper}>
      <div className={styles.buttonWrapper}>
        <button onClick={() => setComponentName('econom')}>Эконом</button>
        <button onClick={() => setComponentName('comfort')}>Комфорт</button>
        <button onClick={() => setComponentName('business')}>Бизнес</button>
        <button onClick={() => setComponentName('minivan')}>Микроавтобусы</button>
        <button onClick={() => setComponentName('microbus')}>Автобусы</button>
      </div>
      {renderComponent(componentName)}
    </div>
  );
};

export default CarouselAuto;