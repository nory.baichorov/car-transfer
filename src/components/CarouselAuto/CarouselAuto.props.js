import focus from '../../assets/econom/focus.webp';
import rapid from '../../assets/econom/rapid.webp';
import vesta from '../../assets/econom/vesta.webp';
import rio from '../../assets/econom/kia_rio.webp';
import polo from '../../assets/econom/vw_polo.webp';
import solaris from '../../assets/econom/solaris.webp';

import camry from '../../assets/comfort/camry.webp';
import optima from '../../assets/comfort/optima.webp';
import passat from '../../assets/comfort/passat.webp';
import mondeo from '../../assets/comfort/mondeo.webp';
import corolla from '../../assets/comfort/corolla.webp';
import octavia from '../../assets/comfort/skoda_octavia.webp';

import bmw5 from '../../assets/bussiness/bmw5.webp';
import a6 from '../../assets/bussiness/audi_a6.webp';
import gle from '../../assets/bussiness/mb_gle.webp';
import quoris from '../../assets/bussiness/quoris.webp';
import eclass from '../../assets/bussiness/mb_eclass.webp';

import vito from '../../assets/minivan/mb_vito.webp';
import sienna from '../../assets/minivan/sienna.webp';
import starex from '../../assets/minivan/starex.webp';
import viano from '../../assets/minivan/mb_viano.webp';
import elysion from '../../assets/minivan/elysion.webp';
import alphard from '../../assets/minivan/alphard.webp';
import multivan from '../../assets/minivan/multivan.webp';

import sprinter from '../../assets/microbus/sprinter.webp';
import crafter from '../../assets/microbus/vw_crafter.webp';

export const EconomProps = [
  {
    carName: 'Ford Focus',
    image: focus
  },
  {
    carName: 'Skoda Rapid',
    image: rapid
  },
  {
    carName: 'Lada Vesta',
    image: vesta
  },
  {
    carName: 'Kia Rio',
    image: rio
  },
  {
    carName: 'Volkswagen Polo',
    image: polo
  },
  {
    carName: 'Hyundai Solaris',
    image: solaris
  },
];

export const ComfortProps = [
  {
    carName: 'Toyota Camry',
    image: camry
  },
  {
    carName: 'Kia Optima',
    image: optima
  },
  {
    carName: 'Volkswagen Passat',
    image: passat
  },
  {
    carName: 'Ford Mondeo',
    image: mondeo
  },
  {
    carName: 'Toyota Corolla',
    image: corolla
  },
  {
    carName: 'Skoda Octavia',
    image: octavia
  },
];

export const BusinessProps = [
  {
    carName: 'Mercedes-Benz E-class',
    image: eclass
  },
  {
    carName: 'BMW 5-Series',
    image: bmw5
  },
  {
    carName: 'Audi A6',
    image: a6
  },
  {
    carName: 'Mercedes-Benz GLE',
    image: gle
  },
  {
    carName: 'Kia Quoris',
    image: quoris
  },
];

export const MinivanProps = [
  {
    carName: 'Mercedes-Benz Vito',
    image: vito
  },
  {
    carName: 'Mercedes-Benz Viano',
    image: viano
  },
  {
    carName: 'Toyota Sienna',
    image: sienna
  },
  {
    carName: 'Hyundai Starex',
    image: starex
  },
  {
    carName: 'Honda Elysion',
    image: elysion
  },
  {
    carName: 'Toyota Alphard',
    image: alphard
  },
  {
    carName: 'Volkswagen Multivan',
    image: multivan
  },
];

export const MicrobusProps = [
  {
    carName: 'Mercedes-Benz Sprinter',
    image: sprinter
  },
  {
    carName: 'Volkswagen Crafter',
    image: crafter
  }
];
