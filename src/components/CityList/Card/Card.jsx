import React from 'react';

import styles from './Card.module.css';

const Card = ({ name, cities, image }) => {
  return (
    <div
      className={styles.wrapper}
      style={{ background: `linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6) ), url(${image}) no-repeat center / 100% 1000px` }}
    >
      <h2 className={styles.h2}>{name}</h2>
      <div className={styles.citiesWrapper}>
        {cities.map(city => {
          return (
            <span key={city.name} className={styles.span}>
              <span>{city.name}</span><span> от {city.price} Р</span>
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default Card;