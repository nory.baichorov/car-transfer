import React from 'react';

import Card from './Card/Card';
import styles from './CityList.module.css';
import { CityListProps } from "./CityList.props";

const CityList = () => {
  return (
    <>
      <h2 className={styles.h2}>
        ПРЕДОСТАВЛЯЕМ СЕРВИС ПАССАЖИРСКИХ ТРАНСПОРТНЫХ УСЛУГ В САМЫЕ ПОПУЛЯРНЫЕ РЕГИОНЫ ИЗ АЭРОПОРТОВ, Ж/Д ВОКЗАЛОВ, АВТОВОКЗАЛОВ И ГОСТИНИЦ
      </h2>
      <div className={styles.wrapper}>
        {CityListProps.map(region => {
          return (
            <Card key={region.name} name={region.name} cities={region.cities} image={region.image} />
          );
        })}
      </div>
    </>
  );
};

export default CityList;