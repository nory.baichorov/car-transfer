import sochi from '../../assets/cities/sochi.webp';
import rostov from '../../assets/cities/rostov.webp';
import elbrus from '../../assets/cities/elbrus.webp';
import grozny from '../../assets/cities/Groznyiy.webp';
import stavropol from '../../assets/cities/stavropol.webp';
import sevastopol from '../../assets/cities/sevastopol.webp';
import kislovodsk from '../../assets/cities/kislovodsk.webp';

export const CityListProps = [
  {
    id: 1,
    name: 'Черноморское побережье',
    image: sochi,
    cities: [
      {
        name: 'Лазаревское',
        price: 11500
      },
      {
        name: 'Джубга',
        price: 9500
      },
      {
        name: 'Архипо-Осиповка',
        price: 9800
      },
      {
        name: 'Анапа',
        price: 11000
      },
      {
        name: 'Адлер',
        price: 13500
      },
      {
        name: 'Витязево',
        price: 11500
      },
      {
        name: 'Новороссийск',
        price: 10800
      },
      {
        name: 'Лермонтово',
        price: 9500
      },
      {
        name: 'Геленджик',
        price: 10600
      },
      {
        name: 'Сочи',
        price: 12800
      },
      {
        name: 'Туапсе',
        price: 10500
      },
    ]
  },
  {
    id: 2,
    name: 'Республика Крым',
    image: sevastopol,
    cities: [
      {
        name: 'Симферополь',
        price: 17000
      },
      {
        name: 'Севастополь',
        price: 19000
      },
      {
        name: 'Феодосия',
        price: 14800
      },
      {
        name: 'Евпатория',
        price: 18500
      },
      {
        name: 'Балаклава',
        price: 18600
      },
      {
        name: 'Ялта',
        price: 18500
      },
      {
        name: 'Керчь',
        price: 13200
      },
    ]
  },
  {
    id: 3,
    name: 'Дальние направления Южного Федерального Округа',
    image: rostov,
    cities: [
      {
        name: 'Краснодар',
        price: 8000
      },
      {
        name: 'Саратов',
        price: 20000
      },
      {
        name: 'Майкоп',
        price: 6000
      },
      {
        name: 'Ростов-на-Дону',
        price: 10000
      },
      {
        name: 'Воронеж',
        price: 20000
      },
      {
        name: 'Элиста',
        price: 6500
      },
      {
        name: 'Волгоград',
        price: 12500
      },
      {
        name: 'Самара',
        price: 28000
      },
      {
        name: 'Астрахань',
        price: 13000
      },
      {
        name: 'Армавир',
        price: 3800
      },
    ]
  },
  {
    id: 4,
    name: 'Северо-Кавказский Федеральный Округ',
    image: grozny,
    cities: [
      {
        name: 'Черкесск',
        price: 2000
      },
      {
        name: 'Назрань',
        price: 4500
      },
      {
        name: 'Беслан',
        price: 4000
      },
      {
        name: 'Карачаевск',
        price: 3000
      },
      {
        name: 'Избербаш',
        price: 10500
      },
      {
        name: 'Белореченск',
        price: 6200
      },
      {
        name: 'Ейск',
        price: 10000
      },
      {
        name: 'Новочеркасск',
        price: 10500
      },
      {
        name: 'Нальчик',
        price: 2200
      },
      {
        name: 'Магас',
        price: 4500
      },
      {
        name: 'Баксан',
        price: 1700
      },
      {
        name: 'Усть-Джегута',
        price: 2300
      },
      {
        name: 'Кизляр',
        price: 8000
      },
      {
        name: 'Адыгея',
        price: 7400
      },
      {
        name: 'Таганрог',
        price: 11500
      },
      {
        name: 'Нарткала',
        price: 2500
      },
      {
        name: 'Владикавказ',
        price: 4500
      },
      {
        name: 'Тырныауз',
        price: 2800
      },
      {
        name: 'Махачкала',
        price: 10000
      },
      {
        name: 'Хасавюрт',
        price: 7500
      },
      {
        name: 'Азов',
        price: 10000
      },
      {
        name: 'Шахты',
        price: 11500
      },
      {
        name: 'Прохладный',
        price: 2500
      },
      {
        name: 'Грозный',
        price: 6800
      },
      {
        name: 'Моздок',
        price: 3500
      },
      {
        name: 'Дербент',
        price: 12000
      },
      {
        name: 'Тихорецк',
        price: 6200
      },
      {
        name: 'Чалтырь',
        price: 10000
      },
      {
        name: 'Батайск',
        price: 10000
      },
    ]
  },
  {
    id: 5,
    name: 'Ставропольский край',
    image: stavropol,
    cities: [
      {
        name: 'Нефтекумск',
        price: 4000
      },
      {
        name: 'Благодарный',
        price: 2900
      },
      {
        name: 'Невинномысск',
        price: 2200
      },
      {
        name: 'Изобильное',
        price: 4500
      },
      {
        name: 'Новопавловск',
        price: 1500
      },
      {
        name: 'Буденновск',
        price: 2900
      },
      {
        name: 'Михайловск',
        price: 3600
      },
      {
        name: 'Ипатово',
        price: 4400
      },
      {
        name: 'Новоселицкое',
        price: 4000
      },
      {
        name: 'Зеленокумск',
        price: 1900
      },
      {
        name: 'Светлоград',
        price: 3500
      },
      {
        name: 'Кочубеевское',
        price: 2400
      },
      {
        name: 'Арзгир',
        price: 4200
      },
      {
        name: 'Георгиевск',
        price: 1200
      },
      {
        name: 'Ставрополь',
        price: 3500
      },
    ]
  },
  {
    id: 6,
    name: 'Горнолыжные курорты',
    image: elbrus,
    cities: [
      {
        name: 'Архыз',
        price: 4000
      },
      {
        name: 'Теберда',
        price: 4000
      },
      {
        name: 'Романтик',
        price: 4000
      },
      {
        name: 'Поляна Азау',
        price: 3800
      },
      {
        name: 'Чегет',
        price: 3700
      },
      {
        name: 'Цей',
        price: 5200
      },
      {
        name: 'Верхний Фиагдон',
        price: 5000
      },
      {
        name: 'Домбай',
        price: 4000
      },
      {
        name: 'Джилы-Су',
        price: 8000
      },
      {
        name: 'Терскол',
        price: 3700
      },
      {
        name: 'Эльбрус',
        price: 3700
      },
      {
        name: 'Байдаево',
        price: 3700
      },
      {
        name: 'Порог неба',
        price: 4500
      },
      {
        name: 'Кармадон',
        price: 5000
      },
    ]
  },
  {
    id: 7,
    name: 'Кавказские Минеральные Воды',
    image: kislovodsk,
    cities: [
      {
        name: 'Минеральные воды',
        price: 500
      },
      {
        name: 'Пятигорск',
        price: 800
      },
      {
        name: 'Лермонтов',
        price: 800
      },
      {
        name: 'Иноземцево',
        price: 700
      },
      {
        name: 'Кисловодск',
        price: 1300
      },
      {
        name: 'Железноводск',
        price: 600
      },
      {
        name: 'Ессентуки',
        price: 1000
      },
      {
        name: 'ст. Ессентукская',
        price: 1100
      },
    ]
  }
]