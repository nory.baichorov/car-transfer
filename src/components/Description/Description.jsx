import React from 'react';

import styles from './Description.module.css';
import phoneLogo from '../../assets/common/phone.svg';
import whatsappLogo from '../../assets/common/whatsapp.svg';
import telegramLogo from '../../assets/common/telegram.svg'


import { DescriptionProps } from './Description.props';
import { isMobile } from "../CarouselAuto/CarouselAuto";

export default function Description({ phone }) {
  const whatsapp = "whatsapp://send?phone=+79887188288";
  const telegram = "tg://resolve?domain=elena_mils";

  return (
    <div className={styles.description} >
      <h1 className={styles.h1}>
        <div>Трансфер из аэропорта, ж/д и автовокзалов</div>
        <div>в любом направлении</div>
      </h1><br/>
      <h3>Высокие стандарты качества</h3><br/>
      <div>
        {DescriptionProps.map(d => {
          return (
            <span key={d.id} className={styles.span}>
              <img src={d.logo} alt="" className={styles.img} />
              {d.text}
            </span>
          );
        })}
      </div><br/>
      <span className={styles.spanWithSvg}>
        <div className={styles.phone}>
          {isMobile ? null : (<img className={styles.svg} src={phoneLogo} alt="" width='20px' height='20px'/>)}
          Связаться с нами можно по номеру {phone}
        </div>
        <div className={styles.messengers}>
          Или напишите нам в мессенджеры:
          <div className={styles.logosWrapper}>
            <a href={whatsapp}>
            <img src={whatsappLogo} alt='WhatsApp' title='WhatsApp'/>
            </a>
            <a href={telegram}>
              <img src={telegramLogo} alt='Telegram' width='25px' height='25px' title='Telegram'/>
            </a>
          </div>
        </div>
      </span>
    </div>
  );
};
