import successLogo from '../../assets/common/success_logo.svg';

export const DescriptionProps = [
  {
    id: 1,
    text: 'Автомобили эконом, среднего и бизнес классов, минивэны, микроавтобусы',
    logo: successLogo
  },
  {
    id: 2,
    text: 'Лучшие условия по стоимости и обсуживанию',
    logo: successLogo
  },
  {
    id: 3,
    text: 'Гарантия лучшей цены',
    logo: successLogo
  },
];