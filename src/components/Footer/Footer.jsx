import React from 'react';

import styles from './Footer.module.css';
import carLogo from '../../assets/common/carTransferLogo.png';

const Footer = ({ phone }) => {
  let date = new Date().getFullYear();
  return (
    <div className={styles.wrapper}>
      <div className={styles.logo}>
        <img src={carLogo} alt="" className={styles.img}/>
      </div>

      <span className={styles.span}>
        &#169; 2018-{date} Все права защищены
      </span>
    </div>
  );
};

export default Footer;