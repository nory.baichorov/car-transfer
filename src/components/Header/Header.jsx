import React, { useState } from 'react';

import styles from './Header.module.css';
import ModalWindow from "./ModalWindow/ModalWindow";
import carLogo from '../../assets/common/carTransferLogo.png';

const Header = ({ carouselRef }) => {
  const [isOpen, setIsOpen] = useState(false);
  const handleOpen = () => setIsOpen(true);
  const handleClose = () => setIsOpen(false);
  const executeScroll = () => carouselRef.current.scrollIntoView();

  return (
    <>
      <ModalWindow open={isOpen} close={handleClose} />
      <div className={styles.wrapper}>
        <div className={styles.logo}>
          <img src={carLogo} alt="" className={styles.img} />
        </div>
        <div className={styles.about}>
          <span onClick={executeScroll}
                className={`${styles.span} ${styles.autopark}`}>
            Автопарк
          </span>
          <span onClick={handleOpen} className={`${styles.span} ${styles.faq}`}>
            Вопросы и ответы
          </span>
        </div>
      </div>
    </>
  );
};

export default Header;