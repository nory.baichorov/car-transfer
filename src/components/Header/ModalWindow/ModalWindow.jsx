import * as React from 'react';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';

import styles from './ModalWindow.module.css';
import { ModalWindowProps } from './ModalWindow.props';
import { isMobile } from "../../CarouselAuto/CarouselAuto";

// Стили MUI по умолчанию
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height: '100%',
  overflow: 'scroll',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

const ModalWindow = ({ open, close }) => {
  return (
    <div>
      <Modal
        open={open}
        onClose={close}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          {ModalWindowProps.map(item => {
            return (
              <>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                  {item.title}
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2, mb: 2 }}>
                  {item.description}
                </Typography>
              </>
            );
          })}
          <div className={styles.buttonWrapper}>
            { isMobile
              ?
              ( <button className={styles.button} onClick={close}>
                  Закрыть
                </button> )
              : null
            }
          </div>
        </Box>
      </Modal>
    </div>
  );
};

export default ModalWindow;